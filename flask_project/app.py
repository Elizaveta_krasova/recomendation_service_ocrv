from flask import Flask, render_template, url_for
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)


@app.route("/home")
@app.route("/")
def index():
    return render_template("index.html", name_professions=["Специалист по анализу данных", "Ведущий специалист по анализу данных", "Старший эксперт по технологиям искусственного интеллекта"])


@app.route("/login")
def login():
    return render_template("login.html")


@app.route("/sign_in")
def sign_in():
    return render_template("sign_in.html")

@app.route("/information")
def information():
    return render_template("information.html", questions=["1. Знание объектно-ориентированного программирования", "2. Знание операционной системы Linux"])


# @app.route("/user/<string:name>/<int:id>")
# def user(name, id):
#     return render_template("index.html")
    # return "user name: " + name + "user id: " + str(id)


if __name__ == '__main__':
    app.run(debug = True)