import time
import requests
from selenium import webdriver
from bs4 import BeautifulSoup


def get_page(url):
    page = requests.get(url)
    return page


def parser(page):
    soup = BeautifulSoup(page,"lxml")
    lst = soup.find_all("a",class_ = "serp-item__title")
    links = []
    count = 0
    for i in lst:
        count += 1
        link = i.get("href")
        links.append(link)
        if count == 5:
            break
    return links


def parser_2(page):
    soup = BeautifulSoup(page,"lxml")
    lst = soup.find_all("div",class_ = "vacancy-section")
    return lst


if __name__ == '__main__':
    web = "/home/elizaveta/choked_python/parse/chromedriver"
    driver = webdriver.Chrome(executable_path=web)

    vacancy = [
        "Младший специалист по анализу данных",
        "Старший эксперт по технологиям искусственного интеллекта",
        "Группа компьтерного зрения",
        "Группа обработки естественного языка",
        "Группа анализа структурированных данных"
    ]
    information_about_vacancy = {}

    for i, vac in enumerate(vacancy):
        driver.get("https://hh.ru/")
        driver.set_window_size(1024, 600)
        driver.maximize_window()
        time.sleep(1)
        driver.find_element("xpath", "//*[@id='a11y-search-input']").send_keys(vac)
        if (i == 0):
            driver.find_element("xpath", "//*[@id='HH-React-Root']/div/div[3]/div[1]/div[1]/div/div/div[2]/div/form/div/div[1]/fieldset/span/a/span").click()
            time.sleep(1)
            driver.find_element("xpath", "//*[@id='HH-React-Root']/div/div[3]/div[1]/div/div/div[1]/form/div[4]/div/div[2]/div/div/div/div/button").click()
            driver.find_element("xpath", "//*[@id='HH-React-Root']/div/div[3]/div[1]/div/div/div[1]/form/div[14]/div[2]/button").click()
        else:
            driver.find_element("xpath","//*[@id='HH-React-Root']/div/div[3]/div[1]/div[1]/div/div/div[2]/div/form/div/div[2]/button").click()

        list_prof = parser(driver.page_source)
        information_about_profession = []
        for i in list_prof:
            driver.get(i)
            information_about_profession.append(parser_2(driver.page_source))

        information_about_vacancy[vac] = information_about_profession

    print(information_about_vacancy)
    driver.close()